﻿using System;
using System.Threading.Tasks;
using Plugin.TextToSpeech;
using Xamarin.Forms;

namespace BasicFormsApp
{
    public class TextToVoiceViewModel : BaseViewModel
    {
        public TextToVoiceViewModel()
        {
        }

		string whatToSay = string.Empty;
		public string WhatToSay
		{
			get { return whatToSay; }
			set { SetProperty(ref whatToSay, value); }
		}


		private Command _speakCommand;
		public Command SpeakCommand => _speakCommand != null ? _speakCommand : (_speakCommand = new Command(DoSpeakCommand));
		private void DoSpeakCommand()
		{
            CrossTextToSpeech.Current.Speak(whatToSay);
		}

	}
}
