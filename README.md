# README #

(Created by Rod Hemphill - July 2017)

### What you will learn ###

* Getting a basic Xamarin forms app running by downloading from a Repo.
* Added a new Forms page.
* Bind the page to a ViewModel conforming to the MVVM design pattern.
* Added a plugin to do some real work.

### Enhancing a basic Xamarin Forms app to add Text to Speech functionality ###

The exercise starts from an already created basic forms app that has two tabs, one with a ListView where selecting a row displays a 
page of details, and one with an 'About' page.

We will add a new tab page with text to speech functionality.

If this is you very first play with Xamarin and Xamarin Forms, although it is simple to create an app from scratch you end up with a blank "What do I do next?" page. Far better to start with an existing app, explore how it's done, butcher it, whatever ...

The purpose of this exercies is to teach you structure of a basic MVVM Xamarin Forms app and how to create a XAML page. The text to 
speach function will be provided using Microsoft Cognative Services.

### Creating the basic app ###

This basic forms app was created using the standard Xamarin Forms example app in Visual Studio for Mac and then converted from a Shared App to a PCL App. 
You could try doing the same - but it was a pain so just use this repo.

Once you have downloaded this repo go to the StartingApp/BasicFormsApp folder and open the BasicFormsApp with Visual Studio (the TextToVoice folder holds what your final code should look like).

I apologise to you Windows Phone UWP people - I haven't added these (a backlog item).

### What do we have? ###

Open the Solution panel and you will see:

* Three 'projects':		BasicFormsApp, BasicFormsApp.Droid, BasicFormsApp.iOS
	The first one is the cross platform code where most of the work happens. 
* The MVVM design pattern has three main components: Models, Views and ViewModels. Appropriately named in the BasicFormsApp project.
	- Model: our example app shows an Item model. The first tab has a ListView that lists Items.
	- View: 5 pages made up of the XAML (the markup language) and "CodeBehind" (any C# formatting or processing code)
	- ViewModel: where you put the buisiness logic.

### Let's run it ###

You'll either need a simulator or a physical device.

Simultors:

* Visual Studio for Mac comes with it's own iOS simulators which work well. The Android ones are a bt slow so I download and install GenyMotion simulators (Genymotion.com). But there are many others.
* For Visual Studio for Windows I also prefer the GenyMotion. You cannot run an iOS app on a Windows machine.

Physical devices:

* Connect your phone to you machine.
* Visual Studio will default to that device.

Building the app:

* First you need to choose whether to build the iOS or Android app. You do this by right clicking on the project (e.g. BasicFormsApp.Droid) and clicking "Set as Startup Project".
* (And while we are here, right click and select "Properties" to see all the setting for the Android build).
* Build and Run by pressing the Start button (the triangle at the top) or use the menu options.

Run it, click each of the tabs and pages, then look for the XAML and ViewModels that do the work.

### Adding a new page ###

Adding the page:

* Right click on the View folder and select Add -> New File
* Select Forms -> Forms ContentPage Xaml and change the name to "TextToVoicePage"
	This will create and new blank page with code behind.

Adding the VewModel:

* Right click on the ViewModel folder and select Add -> New File
* Select General -> Empty Class and change the name to "TextToVoiceViewModel"
* Open it and inherit it from the BaseViewModel. ie make it look like:
	    public class TextToVoiceViewModel : BaseViewModel

Link the View to the ViewModel:

* Open the TextToVoicePage.xaml and add the following below the <ContentPage>
	<ContentPage.BindingContext>
		<vm:TextToVoiceViewModel />
	</ContentPage.BindingContext>
	
* Now define "vm" by adding the following within the <ContentPage> tag, usually before the x:Class propoerty:
	xmlns:vm="clr-namespace:BasicFormsApp;"

Add the new page to the app:

* Open the App.xaml.cs (under the App.xaml)
* In the GoToMainPage method add the new page TextToVoicePage with a title "Text to voice", without an icon.

You can run this now.

### Add Text to Voice functionality ###

Add an input field and button:

* We'll put these to fields within a StackLayout so they stack themselves properly. Place the following within the ContentPage.Content:
        <StackLayout HorizontalOptions="Center" VerticalOptions="Center">
        </StackLayout>

* Within the StackLayout add the input field and button:
            <Entry Placeholder="Enter something to say" Text="{Binding WhatToSay}"/>
            <Button Text="Speak" Command="{Binding SpeakCommand}"/>
	The word "Binding" means bind these with properties and methods within the ViewModel, so let's add these:
	
* Open the TextToVoiceViewModel and add the following property within the class:
		string whatToSay = string.Empty;
		public string WhatToSay
		{
			get { return whatToSay; }
			set { SetProperty(ref whatToSay, value); }
		}
The SetProperty is defined in one of the Helper classes added with the project and handles the two way notification between the View and the ViewModel when that property changes. This is the 'magic'.

* Add the command:
		private Command _speakCommand;
		public Command SpeakCommand => _speakCommand != null ? _speakCommand : (_speakCommand = new Command(async () => await DoSpeakCommand));
		private async Task DoSpeakCommand()
		{
		}

You can run this now. It won't do anything, but you could put a break point in the DoSpeakCommand and confirm that the WhatToSay value is passed in ok.

### Now make it speak ###

Add a NuGet plugin to do the text to voice bit by:

* Right clicking on the Package folder in each of the three projects and select AddPackage.
* Search for the plugin Xam.Plugins.TextToSpeech

* Add the following code to the command above:
           CrossTextToSpeech.Current.Speak(whatToSay);

You are done.


